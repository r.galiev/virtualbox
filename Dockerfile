FROM ubuntu:16.04

ENV DEBIAN_FRONTEND="noninteractive" \
	VIRTUALBOX_VERSION="5.2"

RUN set -ex ;\
	apt-get update ;\
	apt-get -y --no-install-recommends install \
		lsb-release \
		curl \
		ca-certificates \
	;\
	echo "deb http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib" | tee -a /etc/apt/sources.list.d/virtualbox.list ;\
	curl -SsL https://www.virtualbox.org/download/oracle_vbox_2016.asc | apt-key add - ;\
	apt-get update ;\
	apt-get -y --no-install-recommends install \
		virtualbox-$VIRTUALBOX_VERSION \
		kmod \
	;\
	rm -rf /var/lib/apt/lists/*

VOLUME /dev/vboxdrv
